import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from "../router";

const BaseURL = process.env.VUE_APP_API_URL

Vue.use(Vuex)

const state = {
    projectsList: {},
    projectDetail: {},
    useCases: [],
    surfaces: [],
    environments: [],
    // projectCreate: JSON.parse(localStorage.getItem('project')) || {
    //     title: '',
    //     description: '',
    //     phone: '',
    //     address: '',
    //     category: '',
    //     client: '',
    //     tasks: [],
    // }
    createTask: {
        title: '',
        description: '',
        useCase: null,
        budget: 100,
        climate: null,
        surface: null,
        parameters: [
            // {
            //     name: null,
            //     value: null,
            //     area: null
            // }
        ]
        // materials: [
        //     {
        //         seller: null,
        //         quality: null,
        //         price: null
        //     }
        // ],
        // media: []
    },
    square: {
        name: 'square',
        value: null
    },
    color: {
        name: 'color',
        ral: null,
        hex: null,
    },
    taskDetail: {
        id: null,
        title: null,
        description: null,
        budget: null,
        media: [],
        parameters: [],
        use_case: {
            id: null,
            name: null
        },
        surface: {
            id: null,
            name: null
        },
        climate: {
            description: null,
            id: null,
        },
        materials: [],
    }
}

const actions = {
    getProjects: (context) => {
        axios({ url: `${BaseURL}projects/my`, method: 'GET' })
            .then(response => {
                context.commit('setProjects', response.data)
            })
            .catch(error => {
                if(error.response.status === 401){
                    localStorage.removeItem('token')
                    router.push({'path': `/login`});
                }
            })
    },
    getProject: (context, id) => {
        axios({ url: `${BaseURL}projects/${id}`, method: 'GET' })
            .then(response => {
                context.commit('setProject', response.data)
            })
            .catch(error => {
                if(error.response.status === 401){
                    localStorage.removeItem('token')
                    router.push({'path': `/login`});
                }
            })
    },
    createProject: (context, payload) => {
        axios({ url: `${BaseURL}projects`, data: payload, method: 'POST' })
            .then(response => {
                context.commit('setProjects', response.data)
                router.push({'path': `/project/${response.data.id}/add-task`});
            })
            .catch(error => {
                if(error.response.status === 401){
                    localStorage.removeItem('token')
                    router.push({'path': `/login`});
                }
            })
    },
    deleteProject: (context, id) => {
        axios({ url: `${BaseURL}projects/${id}`, method: 'DELETE' })
            .then(response => {
                context.commit('deleteProjectFromList', id)
                router.push({'path': `/projects`});
            })
            .catch(error => {
                if(error.response.status === 401){
                    localStorage.removeItem('token')
                    router.push({'path': `/login`});
                }
            })
    },
    // updateProjectCreate: (context, payload) => {
    //     context.commit('setProjectCreate', payload)
    //     localStorage.setItem('project', JSON.stringify(payload))
    // },
    createTask: (context, payload) => {
        axios({ url: `${BaseURL}projects/${payload.project.id}/tasks`, data: payload.task, method: 'POST' })
            .then(response => {
                context.commit('setProjects', response.data)
                router.push({'path': `/project/${payload.project.id}`});
            })
            .catch(error => {
                if(error.response.status === 401){
                    localStorage.removeItem('token')
                    router.push({'path': `/login`});
                }
            })
    },
    getTask: (context, id) => {
        axios({ url: `${BaseURL}tasks/${id}`, method: 'GET' })
            .then(response => {
                context.commit('setTask', response.data)
            })
            .catch(error => {
                if(error.response.status === 401){
                    localStorage.removeItem('token')
                    router.push({'path': `/login`});
                }
            })
    },
    editTask: (context, payload) => {
        axios({url: `${BaseURL}tasks/${payload.task.id}`, data: payload.task, method: 'PATCH' })
    },
    getUseCases: (context) => {
        axios({ url: `${BaseURL}use-cases`, method: 'GET' })
            .then(response => {
                context.commit('setUseCases', response.data)
            })
            .catch(error => {
                if(error.response.status === 401){
                    localStorage.removeItem('token')
                    router.push({'path': `/login`});
                }
            })
    },
    getSurfaces: (context) => {
        axios({ url: `${BaseURL}surfaces`, method: 'GET' })
            .then(response => {
                context.commit('setSurfaces', response.data)
            })
            .catch(error => {
                if(error.response.status === 401){
                    localStorage.removeItem('token')
                    router.push({'path': `/login`});
                }
            })
    },
    getEnvironments: (context) => {
        axios({ url: `${BaseURL}climates`, method: 'GET' })
            .then(response => {
                context.commit('setEnvironments', response.data)
            })
            .catch(error => {
                if(error.response.status === 401){
                    localStorage.removeItem('token')
                    router.push({'path': `/login`});
                }
            })
    },
    updateTaskUseCase: (context, payload) => {
        context.commit('setTaskUseCase', payload)
        router.push({'path': 'add-task-details'});
    },
    updateTaskName: (context, payload) => {
        context.commit('setTaskName', payload)
        router.push({'path': 'surface'});
    },
    updateTaskSurface: (context, payload) => {
        context.commit('setTaskSurface', payload)
        router.push({'path': 'environment'});
    },
    updateTaskEnvironment: (context, payload) => {
        context.commit('setTaskEnvironment', payload)
        router.push({'path': 'add-square'});
    },
    updateTaskSquare: (context, payload) => {
        context.commit('setTaskSquare', payload)
        router.push({'path': 'color-palette'});
    },
    updateTaskColor: (context, payload) => {
        context.commit('setTaskColor', payload)
        router.push({'path': 'select-paint'});
    },
    updateTaskMaterial: (context, payload) => {
        context.commit('setTaskMaterial', payload)
        router.push({'path': 'select-paint'});
    }
}

const mutations = {
    setProjects(state, projects) {
        state.projectsList = projects
    },
    setProject(state, project) {
        state.projectDetail = project
    },
    deleteProjectFromList: (state, id) => {
        let i = state.projectsList.map(item => item.id).indexOf(id) // find index of your object
        state.projectsList.splice(i, 1)
    },
    // setProjectCreate(state, project) {
    //     state.projectCreate = project
    // },
    setUseCases(state, useCases) {
        state.useCases = useCases
    },
    setSurfaces(state, surfaces) {
        state.surfaces = surfaces
    },
    setEnvironments(state, environments) {
        state.environments = environments
    },
    setTaskUseCase(state, useCase) {
        state.createTask.useCase = useCase.id
    },
    setTaskName(state, payload) {
        state.createTask.title = payload.title
        state.createTask.description = payload.description
    },
    setTaskSurface(state, surface) {
        state.createTask.surface = surface.id
    },
    setTaskEnvironment(state, environment) {
        state.createTask.climate = environment.id
    },
    setTaskSquare(state, square) {
        // state.createTask.parameters[0].name = 'square'
        // state.createTask.parameters[0].value = square
        state.square.value = square
    },
    setTaskColor(state, color) {
        // state.createTask.parameters[1].name = 'color'
        // state.createTask.parameters[1].value = color.ral
        state.color.ral = color.ral
        state.color.hex = color.hex
    },
    setTaskMaterial(state, material) {
        state.createTask.materials.push(material)
    },
    setTask(state, task) {
        state.taskDetail = task
    }
}

const getters = {
    projects(state) {
        return state.projectsList
    },
    project(state) {
        return state.projectDetail
    },
    projectCreate(state) {
        return state.projectCreate
    },
    useCases(state) {
        return state.useCases
    },
    surfaces(state) {
        return state.surfaces
    },
    environments(state) {
        return state.environments
    },
    task(state) {
        return state.taskDetail
    }
}

export const professionalProject = {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};