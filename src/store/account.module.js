import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '../router'


const BaseURL = process.env.VUE_APP_API_URL

Vue.use(Vuex)

const state = {
    status: '',
    token: localStorage.getItem('token') || '',
    user: {}
}

const actions = {
    login: (context, payload) => {
        context.commit('auth_request')
        axios({ url: `${BaseURL}login`, data: payload.user, method: 'POST' })
            .then(resp => {
                const token = resp.data.token
                localStorage.setItem('token', token)
                // Add the following line:
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
                context.dispatch('user')
                context.commit('auth_success', token)
                router.push({'path': '/dashboard'});
            })
            .catch(err => {
                context.commit('auth_error')
                localStorage.removeItem('token')
            })
    },
    register: (context, payload) => {
        return new Promise((resolve, reject) => {
            context.commit('auth_request')
            axios({ url: `${BaseURL}register`, data: payload.user, method: 'POST' })
                .then(resp => {
                    const token = resp.data.token
                    const user = resp.data.user
                    localStorage.setItem('token', token)
                    // Add the following line:
                    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
                    context.dispatch('user')
                    context.commit('auth_success', token)
                    resolve(resp)
                    router.push({'path': `/login`});
                })
                .catch(err => {
                    context.commit('auth_error', err)
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },
    logout: ({ commit }) => {
        return new Promise((resolve, reject) => {
            commit('logout')
            localStorage.removeItem('token')
            delete axios.defaults.headers.common['Authorization']
            resolve()
        })
    },
    user: (context) => {
        axios({ url: `${BaseURL}users/me`, method: 'GET' })
            .then(resp => {
                context.commit('setUser', resp.data)
            })
            .catch(err => {
                context.commit('auth_error')
                localStorage.removeItem('token')
            })
    }
}
const mutations = {
    auth_request(state) {
        state.status = 'loading'
    },
    auth_success(state, token) {
        state.status = 'success'
        state.token = token
    },
    auth_error(state) {
        state.status = 'error'
    },
    logout(state) {
        state.status = ''
        state.token = ''
    },
    setUser(state, user) {
        state.user = user
    }
}
const getters = {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
}


export const account = {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};