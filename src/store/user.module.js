import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '../router'


const BaseURL = process.env.VUE_APP_API_URL

Vue.use(Vuex)

const state = {
    usersList: {},
    userDetail: {}
}

const actions = {
    usersList: (context) => {
        axios({ url: `${BaseURL}users`, method: 'GET' })
            .then(response => {
                context.commit('setUsers', response.data)
            })
            .catch(error => {
                if(error.response.status === 401){
                    localStorage.removeItem('token')
                }
            })
    },
    userDetail: (context, id) => {
        axios({ url: `${BaseURL}users/${id}`, method: 'GET' })
            .then(response => {
                context.commit('setUser', response.data)
            })
            .catch(error => {
                if(error.response.status === 401){
                    localStorage.removeItem('token')
                }
            })
    },
}
const mutations = {
    setUsers(state, users) {
        state.usersList = users
    },
    setUser(state, user) {
        state.userDetail = user
    }
}

export const user = {
    namespaced: true,
    state,
    actions,
    mutations,
};