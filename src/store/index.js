import Vue from 'vue';
import Vuex from 'vuex';

import { account } from './account.module';
import { project } from './project.module';
import { professionalProject } from './professionalProject.module';
import { user } from './user.module';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    getters: {
        account(state) {
            return state.account
        },
        users(state) {
            return state.user.usersList
        },
        user(state) {
            return state.user.userDetail
        }
    },
    modules: {
        account,
        professionalProject,
        user
    }
});