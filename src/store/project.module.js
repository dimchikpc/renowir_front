import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
// import router from '../router'


const BaseURL = process.env.VUE_APP_API_URL

Vue.use(Vuex)

const state = {
    projectsList: {},
    projectDetail: {},
    projectCreate: JSON.parse(localStorage.getItem('projectCreate')) || {
        step: 1,
        title: '',
        description: '',
        category: '',
        currentObject: {
            index: null,
            item: null
        },
        objects: [],
        objectType: 'area',
    }
}

const actions = {
    projectsList: (context) => {
        axios({ url: `${BaseURL}projects`, method: 'GET' })
            .then(response => {
                context.commit('setProjects', response.data)
            })
            .catch(error => {
                if(error.response.status === 401){
                    localStorage.removeItem('token')
                }
            })
    },
    projectDetail: (context, id) => {
        axios({ url: `${BaseURL}projects/${id}`, method: 'GET' })
            .then(response => {
                context.commit('setProject', response.data)
            })
            .catch(error => {
                if(error.response.status === 401){
                    localStorage.removeItem('token')
                }
            })
    },
    updateProjectCreate: (context, payload) => {
        context.commit('setProjectCreate', payload)
        localStorage.setItem('projectCreate', JSON.stringify(payload))
    },
    updateProjectCreateStep: (context, step) => {
        context.commit('setProjectCreateStep', step)
    },
    updateProjectCreateObjects: (context, objects) => {
        context.commit('setProjectObjects', objects)
    },
    updateProjectCreateCurrentObject: (context, payload) => {
        context.commit('setProjectCurrentObject', payload)
    },
    updateProjectCurrentObjectInList: (context, payload) => {
        context.commit('setProjectCurrentObjectInList', payload)
        context.commit('setProjectCurrentObject', { index: null, item: null})
    },
    addObjectToObjectsList: (context, payload) => {
        context.commit('addObjectToObjectsList', payload)
    },
    removeObjectFromObjectsList: (context, index) => {
        context.commit('removeObjectFromObjectsList', index)
    }
}

const mutations = {
    setProjects(state, projects) {
        state.projectsList = projects
    },
    setProject(state, project) {
        state.projectDetail = project
    },
    setProjectCreate(state, project) {
        state.projectCreate = project
    },
    setProjectCreateStep(state, step) {
        state.projectCreate.step = step
    },
    setProjectObjects(state, objects) {
        state.projectCreate.objects = objects
    },
    setProjectCurrentObject(state, payload) {
        state.projectCreate.currentObject = payload
    },
    setProjectCurrentObjectInList(state, payload) {
        state.projectCreate.objects[payload.index] = payload.item
    },
    addObjectToObjectsList(state, payload) {
        state.projectCreate.objects.push(payload)
    },
    removeObjectFromObjectsList(state, index) {
        state.projectCreate.objects.splice(index, 1)
    }
}

const getters = {
    projectsList(state) {
        return state.projectsList
    },
    projectDetail(state) {
        return state.projectDetail
    },
    projectCreate(state) {
        return state.projectCreate
    },
}

export const project = {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};