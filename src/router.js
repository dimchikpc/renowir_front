import Vue from 'vue'
import Router from 'vue-router'
import store from './store/index'
import Home from './views/Home'
import About from './views/About'
// import Dashboard from './views/Dashboard'
import UnderConstruction from './views/UnderConstruction'
import ProfessionalDashboard from './components/Professional/Dashboard'
import ProfessionalProjectsList from './components/Professional/Project/ProjectsList'
import ProfessionalProject from './components/Professional/Project/Project'
import ProfessionalCreateProject from './components/Professional/Project/CreateProject'
import ProfessionalProjectDetails from './components/Professional/Project/ProjectDetails'
import ProfessionalTask from './components/Professional/Task/Task'
import ProfessionalTaskUseCase from './components/Professional/Task/UseCase'
import ProfessionalAddTaskDetails from './components/Professional/Task/AddTaskDetails'
import ProfessionalTaskDetails from './components/Professional/Task/TaskDetails'
import ProfessionalSurface from './components/Professional/Task/Surface'
import ProfessionalEnvironment from './components/Professional/Task/Environment'
import ProfessionalColorSelector from './components/Professional/Task/ColorSelector'
import ProfessionalColorPalette from './components/Professional/Task/ColorPalette'
import ProfessionalSelectPaint from './components/Professional/Task/SelectPaint'
import ProfessionalAddSquare from './components/Professional/Task/AddSquare'
import ProfessionalProjectOffer from './components/Professional/Offer/ProjectOffer'
import ProfessionalTaskOffer from './components/Professional/Offer/TaskOffer'
import ProfessionalTaskOfferDetails from './components/Professional/Offer/TaskOfferDetails'
import ProfessionalContact from './components/Professional/Contact/Contact'
import ProfessionalContactsList from './components/Professional/Contact/ContactsList'
import Login from './views/Login'
import Register from './views/Register'
import ForgotPassword from './views/ForgotPassword'
// import ProjectsList from './components/Project/ProjectsList'
// import ProjectDetail from './components/Project/ProjectDetail'
// import ProjectCreate from './components/Project/ProjectCreate/Index'
// import UsersList from './components/User/UsersList'
// import UserDetail from './components/User/UserDetail'
// import UserManagement from './components/User/UserManagement'
// import NewsList from './components/News/NewsList'
// import OffersList from './components/Offer/OffersList'
// import InvoicesList from './components/Invoice/InvoicesList'
// import AppointmentsList from './components/Appointment/AppointmentsList'
// import Static from './components/Static/Static'
// import Shop from './components/Shop/Shop'
// import Palette from './components/Palette/Palette'
// import Planner from './components/Planner/Planner'

Vue.use(Router)

const scrollBehavior = function (to, from, savedPosition) {
    if (savedPosition) {
        // savedPosition is only available for popstate navigations.
        return savedPosition
    } else {
        const position = {}

        // scroll to anchor by returning the selector
        if (to.hash) {
            position.selector = to.hash

            // specify offset of the element
            if (to.hash === '#anchor2') {
                position.offset = { y: 100 }
            }

            // bypass #1number check
            if (/^#\d/.test(to.hash) || document.querySelector(to.hash)) {
                return position
            }

            // if the returned position is falsy or an empty object,
            // will retain current scroll position.
            return false
        }

        return new Promise(resolve => {
            // check if any matched route config has meta that requires scrolling to top
            if (to.matched.some(m => m.meta.scrollToTop)) {
                // coords will be used if no selector is provided,
                // or if the selector didn't match any element.
                position.x = 0
                position.y = 0
            }

            // wait for the out transition to complete (if necessary)
            this.app.$root.$once('triggerScroll', () => {
                // if the resolved position is falsy or an empty object,
                // will retain current scroll position.
                resolve(position)
            })
        })
    }
}


let router =  new Router({
    mode: 'history',
    base: __dirname,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: {
                requiresAuth: false,
                layout: 'no-sidebar',
                scrollToTop: true
            }
        },
        {
            path: '/about',
            name: 'about',
            component: About,
            meta: {
                requiresAuth: false,
                layout: 'no-sidebar',
                scrollToTop: true
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                requiresAuth: false,
                layout: 'no-sidebar',
                scrollToTop: true
            }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: {
                requiresAuth: false,
                layout: 'no-sidebar',
                scrollToTop: true
            }
        },
        {
            path: '/forgot-password',
            name: 'forgot-password',
            component: ForgotPassword,
            meta: {
                requiresAuth: false,
                layout: 'no-sidebar',
                scrollToTop: true
            }
        },
        {
            path: '/logout',
            name: 'logout',
            beforeEnter (to, from, next) {
                next('/')
            }
        },
        {
            path: '/dashboard',
            name: 'dashboard',
                component: ProfessionalDashboard,
                meta: {
                    requiresAuth: true,
                    layout: 'no-sidebar',
                    scrollToTop: true
                }
        },
        {
            path: '/projects',
            name: 'projects',
            component: ProfessionalProjectsList,
            meta: {
                requiresAuth: true,
                layout: 'no-sidebar',
                scrollToTop: true
            }
        },
        {
            path: '/create-project',
            name: 'create-project',
            component: ProfessionalCreateProject,
            meta: {
                requiresAuth: true,
                layout: 'no-sidebar',
                scrollToTop: true
            }
        },
        {
            path: '/project/:id',
            component: ProfessionalProject,
            children: [
                {
                    path: '/',
                    component: ProfessionalProjectDetails,
                    meta: {
                        requiresAuth: true,
                        layout: 'no-sidebar',
                        scrollToTop: true
                    }
                },
                {
                    path: 'add-task',
                    component: ProfessionalTaskUseCase
                },
                {
                    path: 'add-task-details',
                    component: ProfessionalAddTaskDetails
                },
                {
                    path: 'surface',
                    component: ProfessionalSurface
                },
                {
                    path: 'environment',
                    component: ProfessionalEnvironment
                },
                {
                    path: 'add-square',
                    component: ProfessionalAddSquare,
                },
                {
                    path: 'color-palette',
                    component: ProfessionalColorPalette
                },
                {
                    path: 'color-selector',
                    component: ProfessionalColorSelector
                },
                {
                    path: 'select-paint',
                    component: ProfessionalSelectPaint,
                },
                {
                    path: 'offer',
                    component: ProfessionalProjectOffer,
                },
                {
                    path: 'task/:taskId',
                    component: ProfessionalTask,
                    children: [
                        {
                            path: '/',
                            component: ProfessionalTaskDetails
                        },
                        {
                            // path: 'offer/:offerId',
                            path: 'offer/',
                            component: ProfessionalTaskOffer,
                            children: [
                                {
                                    path: '/',
                                    component: ProfessionalTaskOfferDetails
                                }
                            ]
                        },
                    ]
                },
            ],
            meta: {
                requiresAuth: true,
                layout: 'no-sidebar',
                scrollToTop: true
            }
        },
        {
            path: '/contacts',
            component: ProfessionalContact,
            children: [
                {
                    path: '/',
                    component: ProfessionalContactsList,
                    meta: {
                        requiresAuth: true,
                        layout: 'no-sidebar',
                        scrollToTop: true
                    }
                },
            ]
        },
        {
            path: '/under-construction',
            name: 'under-construction',
            component: UnderConstruction,
            meta: {
                requiresAuth: true,
                layout: 'no-sidebar'
            }
        },
        // {
        //     path: '/add-task',
        //     name: 'add-task',
        //     component: ProfessionalTaskUseCase,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'no-sidebar'
        //     }
        // },
        // {
        //     path: '/add-task-details',
        //     name: 'add-task-details',
        //     component: ProfessionalAddTaskDetails,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'no-sidebar'
        //     }
        // },
        // {
        //     path: '/surface',
        //     name: 'surface',
        //     component: ProfessionalSurface,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'no-sidebar'
        //     }
        // },
        // {
        //     path: '/environment',
        //     name: 'environment',
        //     component: ProfessionalEnvironment,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'no-sidebar'
        //     }
        // },
        // {
        //     path: '/color-selector',
        //     name: 'color-selector',
        //     component: ProfessionalColorSelector,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'no-sidebar'
        //     }
        // },
        // {
        //     path: '/color-palette',
        //     name: 'color-palette',
        //     component: ProfessionalColorPalette,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'no-sidebar'
        //     }
        // },
        // {
        //     path: '/select-paint',
        //     name: 'select-paint',
        //     component: ProfessionalSelectPaint,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'no-sidebar'
        //     }
        // },
        // {
        //     path: '/add-square',
        //     name: 'add-square',
        //     component: ProfessionalAddSquare,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'no-sidebar'
        //     }
        // },
        // {
        //     path: '/offer-details',
        //     name: 'offer-details',
        //     component: ProfessionalOfferDetails,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'no-sidebar'
        //     }
        // },
        // {
        //     path: '/dashboard',
        //     name: 'dashboard',
        //     component: Dashboard,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/projects',
        //     name: 'projects',
        //     component: ProjectsList,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/create-project',
        //     name: 'create-project',
        //     component: ProjectCreate,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/projects/:id',
        //     name: 'project-detail',
        //     component: ProjectDetail,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/users',
        //     name: 'users',
        //     component: UsersList,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/users/:id',
        //     name: 'user-detail',
        //     component: UserDetail,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/user-management',
        //     name: 'user-management',
        //     component: UserManagement,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/news',
        //     name: 'news',
        //     component: NewsList,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/offers',
        //     name: 'offers',
        //     component: OffersList,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/invoices',
        //     name: 'invoices',
        //     component: InvoicesList,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/appointments',
        //     name: 'appointments',
        //     component: AppointmentsList,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/static',
        //     name: 'static',
        //     component: Static,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/shop',
        //     name: 'shop',
        //     component: Shop,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/palette',
        //     name: 'palette',
        //     component: Palette,
        //     meta: {
        //         requiresAuth: true,
        //         layout: 'two-sidebars'
        //     }
        // },
        // {
        //     path: '/planner',
        //     name: 'planner',
        //     component: Planner,
        //     meta: {
        //         requiresAuth: false,
        //         layout: 'no-sidebar'
        //     }
        // },
    ]
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.getters['account/isLoggedIn']) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } else {
        next()
    }
})

export default router