import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import axios from 'axios'

import NoSidebar from "./components/Layout/NoSidebar"
import TwoSidebars from "./components/Layout/TwoSidebars"

import { library } from '@fortawesome/fontawesome-svg-core'
import { faBars, fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


import { Sketch } from 'vue-color'
// import Vuetify from 'vuetify'
// import 'vuetify/dist/vuetify.min.css'
//
// Vue.use(Vuetify)

library.add(faBars, fas, fab)

Vue.component('sketch-picker', Sketch)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.component('no-sidebar-layout', NoSidebar)
Vue.component('two-sidebars-layout', TwoSidebars)

Vue.prototype.$http = axios;

const token = localStorage.getItem('token');
if (token) {
    Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Bearer ' + token
}

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
